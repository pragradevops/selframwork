package co.pragra.dd.framework.drivermanger;

public class BrowserType {
    public final static String CHROME = "chrome";
    public final static String FIREFOX = "firefox";
    public final static String OPERA = "opera";
    public final static String INTERNET_EXPLORER = "ie";
    public final static String EDGE = "edge";
    public final static String SAFARI = "safari";
}
